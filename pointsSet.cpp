#include <iostream>
#include "pointsSet.h"

namespace dip {

using namespace cv;

PointsSet::PointsSet() {
}

PointsSet::~PointsSet(){
    for(container::iterator it = _Map.begin(); it != _Map.end(); ++it) {
        vecPoint* pVec = it->second;
        delete pVec;
    }
}

void PointsSet::AddPoint(int index, Point_<int> point) {
    if(_Map[index]) {
        vecPoint* pVec = _Map[index];
        pVec->push_back(point);
    }
    else {
        vecPoint* pVec = new vecPoint();
        pVec->push_back(point);
        _Map[index] = pVec;
    }
}

void PointsSet::MakeUnion(int ObjIdx1, int ObjIdx2, Mat objMap) {
    vecPoint* pObj1 = _Map[ObjIdx1];
    vecPoint* pObj2 = _Map[ObjIdx2];

    if(pObj2 == NULL)
        return;

    while(!pObj2->empty()) {
        Point_<int> point = pObj2->back();
        pObj2->pop_back();
        pObj1->push_back(point);
        Mobj(objMap, point.x, point.y) = ObjIdx1;
    }
}

int PointsSet::GetMapCount(unsigned int threshold) {
    int count = 0;

    cout << "Map size is " + _Map.size() << endl;

    for(container::iterator it = _Map.begin(); it != _Map.end(); ++it) {
        vecPoint* pVec = it->second;
        if(pVec && (pVec->size() > threshold) ) {
            cout << "Set " << it->first << " has " << pVec->size() << " Items "<< endl;
            count++;
        }
    }

    return count;
}

void PointsSet::Threshold(unsigned int threshold, Mat objMap)
{
    for(container::iterator it = _Map.begin(); it != _Map.end(); ++it) {
        vecPoint* pVec = it->second;
        if(pVec && (pVec->size() < threshold) ) {
            while(!pVec->empty()) {
                Point_<int> point = pVec->back();
                pVec->pop_back();
                Mobj(objMap, point.x, point.y) = 0;
            }
        }
    }
}

}   // namespace dip
