#include <stdio.h>
#include <chrono>
#include <ctime>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "objFinder.h"
#include "medianFilter.h"
#include "blobCounter.h"

namespace dip
{

using namespace cv;
using namespace std;
using namespace chrono;

ObjFinder::ObjFinder() : _filter(MedianFilter(3, 3))
{
    namedWindow("OutputImage", WINDOW_AUTOSIZE);
}

bool ObjFinder::SetSource(Size capSize)
{
    _cap.open(0);
    if (!_cap.isOpened())
    {
         cout << "Failed to open camera" << endl;
         return false;
    }

    cout << "Opened camera" << endl;

    _cap.set(CV_CAP_PROP_FRAME_WIDTH, capSize.width);
    _cap.set(CV_CAP_PROP_FRAME_HEIGHT, capSize.height);

    _cap >> _src;
    cvtColor(_src, _src, COLOR_BGR2GRAY);

    cout << "frame size " << _src.rows + " " + _src.cols << endl;

    _isVideo = true;

    return true;
}

bool ObjFinder::SetSource(const char* source)
{
    _src = imread(source, IMREAD_GRAYSCALE);  /** Load image data into buffer */
    cout << "frame size " << _src.rows + " " + _src.cols << endl;

    _isVideo = false;

    return true;
}

bool ObjFinder::ApplyPrefilters(Mat& src, Mat& dst, bool showResult)
{
    /// Apply median filter
    _filter.SetSource(src);
    _filter.ApplyFilter(dst, showResult);

    /// Apply Otsu's thresholding
    ///threshold(dst, dst, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU );

    return true;
}

int ObjFinder::CountObjs(Mat& img)
{
    /// Run blob counter
    _counter.SetSource(img);
    _counter.Run();

    int objNumber = _counter.GetObjectCount();
    cout << "Counted object number is " << objNumber << endl;

    return objNumber;
}

bool ObjFinder::RunForImage()
{
    Mat result;

    ApplyPrefilters(_src, result, true);

    int countedObjs = CountObjs(result);

    Mat objMap = _counter.GetObjMap();

    char printit[100];
    sprintf(printit,"Object Numbers : %d", countedObjs);
    putText(objMap, printit, cvPoint(10,30), FONT_HERSHEY_PLAIN, 2, cvScalar(255,255,255), 2, 8);

    imshow("OutputImage", objMap);

    waitKey();

    return true;
}

bool ObjFinder::RunForVideo()
{
    int key=0;
    double fps=0.0;
    Mat result, objMap;

    while (1)
    {
        system_clock::time_point start = system_clock::now();
        //for(int a=0;a<10;a++){
        _cap >> _src;
        if( _src.empty() )
            break;

        cvtColor(_src, _src, COLOR_BGR2GRAY);

        ApplyPrefilters(_src, result);

        int countedObjs = CountObjs(result);

        /// objMap = _counter.GetObjMap();

        char printit[100];
        sprintf(printit,"%2.1f, Object Numbers : %d",fps, countedObjs);
        putText(result, printit, cvPoint(10,30), FONT_HERSHEY_PLAIN, 2, cvScalar(0,255,0), 2, 8);
        imshow("OutputImage", result);

        key = waitKey(1);
        if(key==113 || key==27) //either esc or 'q'
            break;

        system_clock::time_point end = system_clock::now();
        double seconds = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
        //fps = 1000000*10.0/seconds;
        fps = 1000000/seconds;
        cout << "frames " << fps << " seconds " << seconds << endl;
    }

    return true;
}

bool ObjFinder::Run()
{
    bool result;

    if(_isVideo)
        result = RunForVideo();
    else
        result = RunForImage();

    return result;
}


}
