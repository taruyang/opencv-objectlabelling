#include <iostream>
#include "blobCounter.h"
#include "medianFilter.h"
#include "objFinder.h"

using namespace std;
using namespace dip;

int main(int argc, char *argv[])
{
    ObjFinder finder;

    if(argc == 2)
    {
        cout << "Blob counting for a still image" << argv[1] << endl;
        finder.SetSource(argv[1]);
    }
    else
    {
        cout << "Blob counting for a video stream from connected a webcam" << endl;
        finder.SetSource();
    }

    finder.Run();

    return 0;
}
