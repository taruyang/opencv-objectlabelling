#ifndef OBJFINDER_H_INCLUDED
#define OBJFINDER_H_INCLUDED

#include "blobCounter.h"
#include "medianFilter.h"

namespace dip
{

using namespace cv;

#define DEFAULT_CAP_W     640
#define DEFAULT_CAP_H     480

class ObjFinder
{
    Mat             _src;
    bool            _isVideo;
    VideoCapture    _cap;
    BlobCounter     _counter;
    MedianFilter    _filter;

    bool            ApplyPrefilters(Mat& src, Mat& dst, bool showResult = false);
    int             CountObjs(Mat& img);
    bool            RunForImage();
    bool            RunForVideo();

public :
    ObjFinder();
    ~ObjFinder(){};

    bool SetSource(Size capSize = Size(DEFAULT_CAP_W, DEFAULT_CAP_H));    /// default input source
    bool SetSource(const char* img);

    bool Run();
};

}

#endif // OBJFINDER_H_INCLUDED
