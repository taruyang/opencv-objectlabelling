#ifndef ACCESSPIXEL_H_INCLUDED
#define ACCESSPIXEL_H_INCLUDED

/** Macro definition to access the each values of image buffer */
/// For gray-scale image
#define Mpixel(image, x, y) ((uchar*)(((image).data) + (y) * ((image).step)))[(x)]
#define MpixelPtr(image, x, y) ((uchar*)(((image).data) + (y) * ((image).step))) + (x)

/// For BGR 24bits image
#define MpixelB_BGR(image, x, y) ((uchar*)(((image).data) + (y) * ((image).step)))[(x) * ((image).channels()) + 0]
#define MpixelG_BGR(image, x, y) ((uchar*)(((image).data) + (y) * ((image).step)))[(x) * ((image).channels()) + 1]
#define MpixelR_BGR(image, x, y) ((uchar*)(((image).data) + (y) * ((image).step)))[(x) * ((image).channels()) + 2]

/// For RGB 24bits image
#define MpixelR_RGB(image, x, y) ((uchar*)(((image).data) + (y) * ((image).step)))[(x) * ((image).channels()) + 0]
#define MpixelG_RGB(image, x, y) ((uchar*)(((image).data) + (y) * ((image).step)))[(x) * ((image).channels()) + 1]
#define MpixelB_RGB(image, x, y) ((uchar*)(((image).data) + (y) * ((image).step)))[(x) * ((image).channels()) + 2]


#endif // ACCESSPIXEL_H_INCLUDED
