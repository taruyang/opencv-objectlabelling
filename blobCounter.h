#ifndef OBJECTLABELLING_H_INCLUDED
#define OBJECTLABELLING_H_INCLUDED
#include "opencv2/opencv.hpp"
#include "pointsSet.h"

namespace dip
{ /// namespace for digital image processing

using namespace cv;

#define TRESHOLD 32

class BlobCounter
{
    Mat             _srcImg;            /// Source image
    Mat             _objMap;            /// Object map
    uint            _objThreshold;      /// minimum object size to be counted.
    PointsSet       _pointSet;          /// A class to handle the sets of pixels

public :
    BlobCounter(uint objThreshold = 60) : _objThreshold(objThreshold) {};
    ~BlobCounter() {};

    void SetSource(const char* source);
    void SetSource(Mat source);
    Mat  GetObjMap();
    bool Run();
    int  GetObjectCount();
};

}

#endif // OBJECTLABELLING_H_INCLUDED
