#ifndef MEDIANFILTER_H_INCLUDED
#define MEDIANFILTER_H_INCLUDED

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace dip
{

using namespace cv;
using namespace std;

class MedianFilter
{
    Mat             _src;           /// Source image
    char            _kW, _kH;       /// kernel Width, kernel Height
    uchar*          _pSet;           /// a bucket to contain the values in the kernel

    void ShowResult(Mat result);    /// display filtered image

public :
    MedianFilter(char kernelW, char KernelH);
    ~MedianFilter();

    void SetSource(const char* source);
    void SetSource(Mat source);
    bool ApplyFilter(Mat& result, bool showResult = false);
};

}
#endif // MEDIANFILTER_H_INCLUDED
