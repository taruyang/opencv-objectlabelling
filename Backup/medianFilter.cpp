#include <algorithm>
#include "medianFilter.h"
#include "accessPixel.h"

namespace dip
{

MedianFilter::MedianFilter(char kernelW, char KernelH) : _kW(kernelW), _kH(KernelH)
{
    _pSet = new uchar[_kW * _kH];
}

MedianFilter::~MedianFilter()
{
    delete [] _pSet;
}

void MedianFilter::SetSource(const char* source)
{
    _src = imread(source, IMREAD_GRAYSCALE);  /** Load image data into buffer */
}

void MedianFilter::SetSource(Mat source)
{
    _src = source;
}

bool MedianFilter::ApplyFilter(Mat& result, bool showResult)
{
    int     w = _src.cols;  /// the width of the source image
    int     h = _src.rows;  /// the height of the source image
    char    hkW = _kW / 2;  /// the length of a kernel side, if the kernel width is 5, this is 2
    char    hkH = _kH / 2;  /// the length of a kernel side

    if(result.size() != _src.size())
        result.create(_src.size(), CV_8UC1);

    for(int i = 0; i < w * h; i++)      /// for performance, this use one for statement
    {
        int y = i / w;
        int x = i % w;
        int idx = 0;
        int uy = y - hkH;
        int dy = y + hkH;
        int lx = x - hkW;
        int rx = x + hkW;

        for(int ky = uy; ky <= dy; ky++)
        {
            if(ky >= 0 && ky < h && lx >= 0 && rx < w)  /// in possible case, use memcpy to reduce time
            {
                memcpy(&_pSet[idx], MpixelPtr(_src, lx, ky), _kW);
                idx += _kW;
                continue;
            }

            for(int kx = lx; kx <= rx; kx++)    /// this is for edge case
            {
                int tx = (kx < 0 || kx >= w) ? x : kx;
                int ty = (ky < 0 || ky >= h) ? y : ky;

                _pSet[idx++] = Mpixel(_src, tx, ty);
            }
        }

        /// Take a value in the middle from sorted set
///        sort (_pSet, _pSet + idx);
        Mpixel(result, x, y) = _pSet[(_kW * _kH) / 2 + 1]; /// Take a value in the middle from sorted bucket
    }

    if(showResult)
        ShowResult(result);

    return true;
}

void MedianFilter::ShowResult(Mat result)
{
    namedWindow("Original", CV_WINDOW_AUTOSIZE);
    imshow("Original", _src);

    namedWindow("Filtered Image", CV_WINDOW_AUTOSIZE);
    imshow("Filtered Image", result);

    waitKey();
}


}
