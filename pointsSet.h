#ifndef POINTS_H_INCLUDED
#define POINTS_H_INCLUDED

#include <unordered_map>
#include "opencv2/opencv.hpp"
#include "accessPixel.h"

#define Mobj(image, x, y) ((int*)(((image).data) + (y) * ((image).step)))[(x)]

namespace dip {     /// namespace for digital image processing

using namespace std;
using namespace cv;
typedef deque< Point_<int> > vecPoint;
typedef unordered_map< int, vecPoint* > container;

class PointsSet {
    unordered_map< int, vecPoint* >  _Map;

public:
    PointsSet();
    ~PointsSet();
    void AddPoint(int index, Point_<int> point);
    void MakeUnion(int indexSet1, int indexSet2, Mat objMap);
    int GetMapCount(unsigned int threshold);
    void Threshold(unsigned int threshold, Mat objMap);
};  // PointsSet

}

#endif // POINTS_H_INCLUDED
