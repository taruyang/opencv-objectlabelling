# README #

This program written in C++ implements a median filter to eliminate pepper and salt noise and a blob
counter using the object labelling algorithm. 

## Description ##

- The objective of this program is to correctly count and show a filtered image of every blob. 
- The program also shows an image for every step. 
- It takes one argument for a static image file. 
  E.g.: ./ObjectLabelling image1.jpg  
- with no arguments, it automatically starts the web-cam to count objects in the captured image.
- The number of objects are printed on the the image is static

## What have been done to enhance performance ##

To enhance performance, the below points are applied based on the performance measurement.

### 1. [Blob Counter] Proper containers.###
**1-1.  deque instead of vector or array**

Firstly, I have used the vector to handle pixels which are in a object.
And have checked which container is faster than the other.
After measuring C++ standard containers and C array,
I have found that the deque is faster than the others.
This is because all operations required are push_back and pop_back.
For this purpose, the deque would be the best tools.
I observed there is around 1 fps enhancement with deque.

**1-2.  unordered_map instead of map or set**

Also, used unordered_map instead of map or set which use sort operation when an item is 
added. 	The cost of unordered_map to insert and remove is cheaper han the other container 

### 2. [MedianFilter] The sort operation ###
When I use the video input from Web Cam, the initial performance was not good  
than I expected. I have measured to figure out which point was the bottleneck of
performance. It was the sort operation. So I have changed it with my own implementation
and the other sort method.
However, there has been no big difference. 
So, I slightly modified the algorithm not to call the sort function as least as possible.
Only call the sort operation when there is a noise pixel which has distinct value.
If there is no distinct pixel in a kernel mask, the original pixel value is used without sorting.
With this, around 2 fps improvement is observed in my test environment.

### 3. The release mode ###
When the release mode for compiling is used for compiling, there is a significant improvement.
The difference is around 2 fps in my test environment.

To enhance correctness, the below point is applied based on experiment.

### 4. [Blob Counter] appropriate threshold ###

There has been fine adjustment process to find proper threshold values. 
Based on experiment, current values have been adopted.

### Screenshot ###
![ObjectLabelling.png](https://bitbucket.org/repo/yp8anRL/images/562490772-ObjectLabelling.png)

### Infortant Information ###

* The used blob counter algorithm is came from an article.