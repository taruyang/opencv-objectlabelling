#include <algorithm>
#include "medianFilter.h"
#include "accessPixel.h"

namespace dip
{

MedianFilter::MedianFilter(char kernelW, char KernelH) : _kW(kernelW), _kH(KernelH)
{
    _pSet = new uchar[_kW * _kH];
}

MedianFilter::~MedianFilter()
{
    delete [] _pSet;
}

void MedianFilter::SetSource(const char* source)
{
    _src = imread(source, IMREAD_GRAYSCALE);  /** Load image data into buffer */
}

void MedianFilter::SetSource(Mat source)
{
    _src = source;
}

bool MedianFilter::ApplyFilter(Mat& result, bool showResult)
{
    int     w = _src.cols;  /// the width of the source image
    int     h = _src.rows;  /// the height of the source image
    char    hkW = _kW / 2;  /// the length of a kernel side, if the kernel width is 5, this is 2
    char    hkH = _kH / 2;  /// the length of a kernel side

    if(result.size() != _src.size())
        result.create(_src.size(), CV_8UC1);

    for(int i = 0; i < w * h; i++)      /// for performance, this use one for statement
    {
        int y = i / w;
        int x = i % w;
        int idx = 0;
        int uy = y - hkH;
        int dy = y + hkH;
        int lx = x - hkW;
        int rx = x + hkW;
        uchar pixel = 0;

        int prev = -1, diff = 0;
        for(int ky = uy; ky <= dy; ky++)
        {
            for(int kx = lx; kx <= rx; kx++)
            {
                int tx = (kx < 0 || kx >= w) ? x : kx;
                int ty = (ky < 0 || ky >= h) ? y : ky;

                _pSet[idx] = Mpixel(_src, tx, ty);

                if(prev)
                {
                    int tmp = prev - _pSet[idx];
                    tmp = (tmp) ? tmp : -tmp;
                    diff = (tmp > diff) ? tmp : diff;
                }

                prev = _pSet[idx];
                idx++;
            }
        }

        /// if the maximum difference among pixel in the kernel is small,
        /// it means there is not a noise. In this case, do not call the sort function.
        /// In my experience, the function makes the output performance slow around 1 fps.
        if(diff > 20)
        {
            sort (_pSet, _pSet + idx);
            pixel = _pSet[(_kW * _kH) / 2 + 1]; /// Take a value in the middle from sorted bucket
        }
        else
            pixel = Mpixel(_src, x, y);

        Mpixel(result,x,y) = pixel;
    }

    if(showResult)
        ShowResult(result);

    return true;
}

void MedianFilter::ShowResult(Mat result)
{
    namedWindow("Original", CV_WINDOW_AUTOSIZE);
    imshow("Original", _src);

    namedWindow("Filtered Image", CV_WINDOW_AUTOSIZE);
    imshow("Filtered Image", result);
}


}
