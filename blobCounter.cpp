#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "blobCounter.h"
#include "pointsSet.h"
#include "accessPixel.h"

namespace dip       /// namespace for digital image processing
{

using namespace std;
using namespace cv;

void BlobCounter::SetSource(const char* source)
{
    _srcImg = imread(source, IMREAD_GRAYSCALE);  /** Load image data into buffer */
    _objMap.create(_srcImg.size(), CV_32SC1);
    _objMap = Scalar(0);
}

void BlobCounter::SetSource(Mat source)
{
    _srcImg = source;
    _objMap.create(_srcImg.size(), CV_32SC1);
    _objMap = Scalar(0);
}

/** Main function to do algorithm */
bool BlobCounter::Run()
{
    int objIdx = 0; /// for object counting

    /** 4 adjacency method */
    for(int y = 1; y < _srcImg.rows; y++)
    {
        for(int x = 1; x < _srcImg.cols; x++)
        {
            if(Mpixel(_srcImg, x, y) > TRESHOLD)    /// If a pixel is in an object
            {
                /// check near pixels to confirm if this pixel is connected with the other objects
                if(Mpixel(_srcImg, x - 1, y) > TRESHOLD || Mpixel(_srcImg, x, y - 1) > TRESHOLD )
                {
                    int leftObjIdx = Mobj(_objMap, x - 1, y);
                    int upperObjIdx = Mobj(_objMap, x, y - 1);

                    if(leftObjIdx)
                    {
                        _pointSet.AddPoint(leftObjIdx, Point_<int>(x,y));
                        Mobj(_objMap, x, y) = leftObjIdx;
                    }
                    else if(upperObjIdx)
                    {
                        _pointSet.AddPoint(upperObjIdx, Point_<int>(x,y));
                        Mobj(_objMap, x, y) = upperObjIdx;
                    }

                    if((leftObjIdx != upperObjIdx) && (leftObjIdx) && (upperObjIdx))
                        _pointSet.MakeUnion(leftObjIdx, upperObjIdx, _objMap);
                }
                else
                {
                    objIdx++;
                    _pointSet.AddPoint(objIdx, Point_<int>(x,y));
                    Mobj(_objMap, x, y) = objIdx;
                }
            }
         }
    }

    return true;
}

Mat BlobCounter::GetObjMap()
{
    Mat         dbgImg;

    dbgImg.create(_srcImg.size(), CV_8UC3);
    dbgImg = Scalar(0,0,0);

    _pointSet.Threshold(_objThreshold, _objMap);

    for(int y = 0; y < _objMap.rows; y++)
    {
        for(int x = 0; x < _objMap.cols; x++)
        {
            int objIdx = Mobj(_objMap, x, y);
            ///int objIdx = _objMap.at<int>(y,x);
            if(objIdx == 0)
                continue;

            objIdx %= 255;

            int B = (objIdx & 0x07) ? objIdx * 5 : 0;
            int G = (objIdx & 0x18) ? objIdx * 5 : 0;
            int R = (objIdx & 0x60) ? objIdx * 5 : 0;

            B %= 255;G %= 255;R %= 255;

            MpixelB_BGR(dbgImg, x, y) = (uchar)B;
            MpixelG_BGR(dbgImg, x, y) = (uchar)G;
            MpixelR_BGR(dbgImg, x, y) = (uchar)R;
        }
    }

    return dbgImg;
}

int BlobCounter::GetObjectCount()
{
    return _pointSet.GetMapCount(_objThreshold);
}

}
